// Fill out your copyright notice in the Description page of Project Settings.

#include "SpaceLine.h"
#include "Engine/Level.h"
#include "PlayerCamera.h"
#include "PlayerSpriteShip.h"



// Sets default values
APlayerCamera::APlayerCamera()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SphereComponent = CreateDefaultSubobject<USphereComponent>(FName("SphereComponent"));

	RootComponent = SphereComponent;

	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(FName("SpringArm"));
	SpringArmComponent->AttachTo(SphereComponent);
	SpringArmComponent->TargetArmLength = 2000;
	SpringArmComponent->SetRelativeRotation(FRotator(-90, 0, 0));
	SpringArmComponent->bDoCollisionTest = false;
	
	

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(FName("CameraComponent"));
	CameraComponent->AttachTo(SpringArmComponent);
	CameraComponent->ProjectionMode = ECameraProjectionMode::Orthographic;
	CameraComponent->bConstrainAspectRatio = true;
	CameraComponent->OrthoWidth = 3000;
	
	CameraComponent->AspectRatio = 0.6;
	
	AudioComponent = CreateDefaultSubobject<UAudioComponent>(FName("AudioComponent"));
	AudioComponent->AttachTo(RootComponent);

	static ConstructorHelpers::FObjectFinder<USoundBase> SoundAmbientRes(TEXT("/Game/SpaceLine/Sounds/exodus_Cue.exodus_Cue"));

	AudioComponent->SetSound(SoundAmbientRes.Object);
}

// Called when the game starts or when spawned
void APlayerCamera::BeginPlay()
{
	Super::BeginPlay();
	if (Controller)
	{
		Cast<APlayerController>(Controller)->bShowMouseCursor = true;
		Cast<APlayerController>(Controller)->bEnableClickEvents = true;
		Cast<APlayerController>(Controller)->bEnableMouseOverEvents = true;
		FVector spawnpos(0, 0, 100);
		FRotator spawnrot(0, -90, 90);
		PlayerShip = (APlayerSpriteShip*)GetWorld()->SpawnActor(APlayerSpriteShip::StaticClass(), &spawnpos, &spawnrot);
		
		ShipInitialized = true;
	}
}

void APlayerCamera::PostLoad()
{
	Super::PostLoad();

	
}

// Called every frame
void APlayerCamera::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	//if (bMousePressed)
	UpdatePlayerShipPos();
}

// Called to bind functionality to input
void APlayerCamera::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
	//InputComponent->BindAction(FName("MouseClick"), IE_Pressed, this, &APlayerCamera::OnPressed);
	//InputComponent->BindAction(FName("MouseClick"), IE_Released, this, &APlayerCamera::OnReleased);
}

void APlayerCamera::OnPressed()
{
	bMousePressed = true;
}

void APlayerCamera::OnReleased()
{
	bMousePressed = false;
}

void APlayerCamera::UpdatePlayerShipPos()
{
	
	if (!PlayerShip.IsValid())
		return;
	
	FHitResult hit;
	Cast<APlayerController>(Controller)->GetHitResultUnderFingerByChannel(ETouchIndex::Type::Touch1,ETraceTypeQuery::TraceTypeQuery1, false, hit);
	Cast<APlayerController>(Controller)->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery1,false,hit);

	if (hit.ImpactPoint.Y > 1500)
		hit.ImpactPoint.Y = 1500;
	if (hit.ImpactPoint.Y<-1500)
		hit.ImpactPoint.Y = -1500;
	if (hit.ImpactPoint.X>2500)
		hit.ImpactPoint.X = 2500;
	if (hit.ImpactPoint.X<-2500)
		hit.ImpactPoint.X = -2500;
	PlayerShip->SetActorLocation(FVector(hit.ImpactPoint.X, hit.ImpactPoint.Y, 100),true);
}

bool APlayerCamera::ShipIsDead()
{
	
	if (!PlayerShip.IsValid())
		return true;

	return PlayerShip->isDead();
}

