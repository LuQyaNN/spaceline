// Fill out your copyright notice in the Description page of Project Settings.

#include "SpaceLine.h"
#include "Bullet.h"


// Sets default values
ABullet::ABullet()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	GetRenderComponent()->bGenerateOverlapEvents = true;
	GetRenderComponent()->SetNotifyRigidBodyCollision(true);

	AudioComponent = CreateDefaultSubobject<UAudioComponent>(FName("AudioComponent"));
	
	AudioComponent->AttachTo(GetRenderComponent());
	
}

// Called when the game starts or when spawned
void ABullet::BeginPlay()
{
	Super::BeginPlay();
	GetWorld()->GetTimerManager().SetTimer(KillTimer, this, &ABullet::Destroy, 3, false);
	
}

// Called every frame
void ABullet::Tick( float DeltaTime )
{
	
	Super::Tick( DeltaTime );

}

void ABullet::Destroy()
{
	Super::Destroy();
}

void ABullet::OnSpawned(FVector Location, FRotator Direction)
{

}