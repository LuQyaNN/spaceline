// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/HUD.h"
#include "PlayerHUD.generated.h"

/**
 * 
 */
UCLASS()
class SPACELINE_API APlayerHUD : public AHUD
{
	GENERATED_BODY()

public:
	APlayerHUD();
		void DrawHUD() override;
		FTexture* ArmorTexRes;
		UFont* Font;
		void NotifyHitBoxBeginCursorOver(FName BoxName) override;
		void NotifyHitBoxEndCursorOver(FName BoxName) override;
		bool OverRestartButton, OverMenuButton;
		bool LevelComplete;
		void NotifyHitBoxClick(FName BoxName) override;
		void DrawHealth(TWeakObjectPtr<class AEnemy>);
		UAudioComponent* AudioComponent;
};
