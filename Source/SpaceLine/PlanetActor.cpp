// Fill out your copyright notice in the Description page of Project Settings.

#include "SpaceLine.h"
#include "PlanetActor.h"


APlanetActor::APlanetActor()
{
	PrimaryActorTick.bCanEverTick = true;
	static ConstructorHelpers::FObjectFinder<UPaperFlipbook> Sprite(TEXT("/Game/SpaceLine/Planets/planet_Flipbook.planet_Flipbook"));
	GetRenderComponent()->SetFlipbook(Sprite.Object);
	GetRenderComponent()->AddWorldRotation(FRotator(0, 0, 90));
	GetRenderComponent()->SetWorldScale3D(FVector(4, 4, 4));

}

void APlanetActor::Tick(float delta)
{
	Super::Tick(delta);
	AddActorWorldOffset(FVector(-30  * delta, 0, 0));
}

