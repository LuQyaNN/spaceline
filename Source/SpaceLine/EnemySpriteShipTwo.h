// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Enemy.h"
#include "EnemySpriteShipTwo.generated.h"

/**
 * 
 */
UCLASS()
class SPACELINE_API AEnemySpriteShipTwo : public AEnemy
{
	GENERATED_BODY()
public:
	AEnemySpriteShipTwo();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	FTimerHandle FireTimer;
	FTimerHandle StageTimer;
	int32 FireStage;
	void BeginFireStage();
	virtual void Fire() override;
	
	
};
