// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Bullet.h"
#include "MachineGunBullet.generated.h"

/**
 * 
 */
UCLASS()
class SPACELINE_API AMachineGunBullet : public ABullet
{
	GENERATED_BODY()
	
public:
	AMachineGunBullet();

	virtual void OnSpawned(FVector Location, FRotator Direction) override;
	
	void Tick(float delta) override;
	//void ReceiveActorBeginOverlap(AActor* OtherActor);
	void NotifyHit(UPrimitiveComponent * MyComp,AActor * Other,UPrimitiveComponent * OtherComp,bool bSelfMoved,FVector HitLocation,FVector HitNormal,FVector NormalImpulse,const FHitResult & Hit)override;

	UParticleSystem* ParticleSystem;
};
