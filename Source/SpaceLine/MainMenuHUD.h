// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/HUD.h"
#include "MainMenuHUD.generated.h"

/**
 * 
 */
UCLASS()
class SPACELINE_API AMainMenuHUD : public AHUD
{
	GENERATED_BODY()
	
public:
	AMainMenuHUD();
	void DrawHUD() override;
	void NotifyHitBoxClick(FName BoxName) override;
	
	void NotifyHitBoxBeginCursorOver(FName BoxName) override;
	void NotifyHitBoxEndCursorOver(FName BoxName) override;

	bool OverStartButton, OverExitButton;
	UAudioComponent* AudioComponent;
	UFont* Font;


};
