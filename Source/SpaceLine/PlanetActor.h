// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaperFlipbookActor.h"
#include "PlanetActor.generated.h"

/**
 * 
 */
UCLASS()
class SPACELINE_API APlanetActor : public APaperFlipbookActor
{
	GENERATED_BODY()
public:

	APlanetActor();

	void Tick(float delta) override;
	
	
};
