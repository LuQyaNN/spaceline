// Fill out your copyright notice in the Description page of Project Settings.

#include "SpaceLine.h"
#include "MainMenuGameMode.h"
#include "MainMenuPawn.h"
#include "MainMenuHUD.h"

AMainMenuGameMode::AMainMenuGameMode()
{
	DefaultPawnClass = AMainMenuPawn::StaticClass();
	HUDClass = AMainMenuHUD::StaticClass();
}


