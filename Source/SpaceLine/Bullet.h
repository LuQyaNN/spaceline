// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaperFlipbookActor.h"
#include "Bullet.generated.h"

UCLASS()
class SPACELINE_API ABullet : public APaperFlipbookActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABullet();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UAudioComponent* AudioComponent;

	virtual void OnSpawned(FVector Location,FRotator Direction);
	FTimerHandle KillTimer;
	
	void Destroy();
};
