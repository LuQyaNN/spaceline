// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaperFlipbookActor.h"
#include "BonusArmor.generated.h"

/**
 * 
 */
UCLASS()
class SPACELINE_API ABonusArmor : public APaperFlipbookActor
{
	GENERATED_BODY()
public:

		ABonusArmor();

		void Tick(float delta) override;

		void NotifyHit(UPrimitiveComponent * MyComp, AActor * Other, UPrimitiveComponent * OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult & Hit)override;
	
};
