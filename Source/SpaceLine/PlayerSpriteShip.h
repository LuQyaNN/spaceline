// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaperFlipbookActor.h"
#include "PlayerSpriteShip.generated.h"

/**
 * 
 */
UCLASS()
class SPACELINE_API APlayerSpriteShip : public APaperFlipbookActor
{
	GENERATED_BODY()

public:

	APlayerSpriteShip();
	void Fire();
	void FireRocket();
	virtual void BeginPlay() override;
	int32 Health;
	bool AddHealth(int32 num);
	bool bImmortality;
	UPaperFlipbook* ImmortalSprite, *Sprite;
	UAudioComponent* AudioComponent;
	UPROPERTY(EditAnywhere)
	int32 WeaponLvl;
	void NotifyActorBeginOverlap(AActor* OtherActor) override;
	bool AddWeaponLvl(int32 num);
	bool isDead();
	TWeakObjectPtr<class AShipTurret> turret;
	FTimerHandle FireTimer, RocketFireTimer, ImmortalityTimer;
	void EnableImmortality();
	void DisableImmortality();
	
};
