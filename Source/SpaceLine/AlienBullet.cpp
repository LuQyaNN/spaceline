// Fill out your copyright notice in the Description page of Project Settings.

#include "SpaceLine.h"
#include "AlienBullet.h"
#include "PlayerSpriteShip.h"

AAlienBullet::AAlienBullet()
{

	PrimaryActorTick.bCanEverTick = true;
	static ConstructorHelpers::FObjectFinder<UPaperFlipbook> Flipbook(TEXT("/Game/SpaceLine/Shots/AlienGun/Alien_Bullet.Alien_Bullet"));
	GetRenderComponent()->SetFlipbook(Flipbook.Object);
	GetRenderComponent()->bGenerateOverlapEvents = true;
	GetRenderComponent()->SetNotifyRigidBodyCollision(true);
	GetRenderComponent()->SetCollisionProfileName(TEXT("AlienBullet"));

	static ConstructorHelpers::FObjectFinder<USoundBase> SoundShotRes(TEXT("/Game/SpaceLine/Sounds/alien_shot_Cue.alien_shot_Cue"));
	AudioComponent->SetSound(SoundShotRes.Object);
}

void AAlienBullet::OnSpawned(FVector Location, FRotator Direction)
{

}

void AAlienBullet::Tick(float delta)
{
	Super::Tick(delta);
	if (bSinMove)
	{
		SinMoveTime += 0.1;
		float tmp = FMath::Sin(SinMoveTime);
		AddActorLocalOffset(FVector(tmp * 10 , 0, 30 * 100 * delta), true);
	}
	else
	{
		AddActorLocalOffset(FVector(0, 0, 30 * 100 * delta), true);
	}
}

void AAlienBullet::NotifyHit(UPrimitiveComponent * MyComp, AActor * Other, UPrimitiveComponent * OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult & Hit)
{
	Super::NotifyHit(MyComp, Other, OtherComp, bSelfMoved, HitLocation, HitNormal, NormalImpulse, Hit);
	if (Cast<APlayerSpriteShip>(Other) != nullptr)
	{
		Cast<APlayerSpriteShip>(Other)->AddHealth(-1);
		Destroy();
	}
}



