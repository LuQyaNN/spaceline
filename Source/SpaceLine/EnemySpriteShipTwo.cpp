// Fill out your copyright notice in the Description page of Project Settings.

#include "SpaceLine.h"
#include "EnemySpriteShipTwo.h"
#include "AlienBullet.h"


AEnemySpriteShipTwo::AEnemySpriteShipTwo()
{


	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	static ConstructorHelpers::FObjectFinder<UPaperFlipbook> Sprite(TEXT("/Game/SpaceLine/Ships/Alien_2/alien.alien"));
	GetRenderComponent()->SetFlipbook(Sprite.Object);
	GetRenderComponent()->SetWorldScale3D(FVector(3, 3, 3));
	//SetActorRotation(FRotator(0, -90, -90));
	Health = 6;
	MaxHealth = 6;
	GetRenderComponent()->bGenerateOverlapEvents = true;
	GetRenderComponent()->SetNotifyRigidBodyCollision(true);


}

// Called when the game starts or when spawned
void AEnemySpriteShipTwo::BeginPlay()
{
	Super::BeginPlay();
	GetWorld()->GetTimerManager().SetTimer(FireTimer, this, &AEnemySpriteShipTwo::BeginFireStage, 2, true);
}

// Called every frame
void AEnemySpriteShipTwo::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AEnemySpriteShipTwo::BeginFireStage()
{
	FireStage = 0;
	GetWorld()->GetTimerManager().SetTimer(StageTimer, this, &AEnemySpriteShipTwo::Fire, 0.2, true);

}

void AEnemySpriteShipTwo::Fire()
{
	FVector loc = GetActorLocation();
	FRotator rot = GetActorRotation() - FRotator(0, 40, 0) + FRotator(0, 20 * FireStage, 0);
	GetWorld()->SpawnActor(AAlienBullet::StaticClass(), &loc, &rot);
	if (FireStage == 4)
		GetWorld()->GetTimerManager().ClearTimer(StageTimer);
	FireStage++;
}



