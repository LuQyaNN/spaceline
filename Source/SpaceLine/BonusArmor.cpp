// Fill out your copyright notice in the Description page of Project Settings.

#include "SpaceLine.h"
#include "BonusArmor.h"
#include "PlayerSpriteShip.h"


ABonusArmor::ABonusArmor()
{
	
	PrimaryActorTick.bCanEverTick = true;
	static ConstructorHelpers::FObjectFinder<UPaperFlipbook> Sprite(TEXT("/Game/SpaceLine/Bonuses/Armor/shield_PNG.shield_PNG"));
	GetRenderComponent()->SetFlipbook(Sprite.Object);
	GetRenderComponent()->bGenerateOverlapEvents = true;
	GetRenderComponent()->SetNotifyRigidBodyCollision(true);
	GetRenderComponent()->SetWorldScale3D(FVector(0.3, 0.3, 0.3));
	GetRenderComponent()->SetCollisionProfileName(TEXT("Bonus"));
}

void ABonusArmor::Tick(float delta)
{
	Super::Tick(delta);
	AddActorLocalOffset(FVector(0, 0, -30 * 50 * delta),true);

}

void ABonusArmor::NotifyHit(UPrimitiveComponent * MyComp, AActor * Other, UPrimitiveComponent * OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult & Hit)
{
	Super::NotifyHit(MyComp, Other, OtherComp, bSelfMoved, HitLocation, HitNormal, NormalImpulse, Hit);
	if (Cast<APlayerSpriteShip>(Other) != nullptr)
	{
		Cast<APlayerSpriteShip>(Other)->AddHealth(1);
		Destroy();
	}
}


