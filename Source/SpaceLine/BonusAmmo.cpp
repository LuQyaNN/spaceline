// Fill out your copyright notice in the Description page of Project Settings.

#include "SpaceLine.h"
#include "BonusAmmo.h"
#include "PlayerSpriteShip.h"

ABonusAmmo::ABonusAmmo()
{
	
	PrimaryActorTick.bCanEverTick = true;
	static ConstructorHelpers::FObjectFinder<UPaperFlipbook> Sprite(TEXT("/Game/SpaceLine/Bonuses/Ammo/Tul_Ammo_9mm_Frontview_1600x.Tul_Ammo_9mm_Frontview_1600x"));
	GetRenderComponent()->SetFlipbook(Sprite.Object);
	GetRenderComponent()->bGenerateOverlapEvents = true;
	GetRenderComponent()->SetNotifyRigidBodyCollision(true);
	GetRenderComponent()->SetWorldScale3D(FVector(0.3, 0.3, 0.3));
	GetRenderComponent()->SetCollisionProfileName(TEXT("Bonus"));
}

void ABonusAmmo::Tick(float delta)
{
	Super::Tick(delta);
	AddActorLocalOffset(FVector(0, 0, -30 * 50 * delta), true);

}

void ABonusAmmo::NotifyHit(UPrimitiveComponent * MyComp, AActor * Other, UPrimitiveComponent * OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult & Hit)
{
	Super::NotifyHit(MyComp, Other, OtherComp, bSelfMoved, HitLocation, HitNormal, NormalImpulse, Hit);
	if (Cast<APlayerSpriteShip>(Other) != nullptr)
	{
		if (!Cast<APlayerSpriteShip>(Other)->AddWeaponLvl(1))
		{
			float TimerRate = GetWorld()->GetTimerManager().GetTimerRate(Cast<APlayerSpriteShip>(Other)->FireTimer);
			GetWorld()->GetTimerManager().SetTimer(Cast<APlayerSpriteShip>(Other)->FireTimer, Cast<APlayerSpriteShip>(Other), &APlayerSpriteShip::Fire, TimerRate*0.9, true);
		}
		Destroy();
	}
}





