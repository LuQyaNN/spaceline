// Fill out your copyright notice in the Description page of Project Settings.

#include "SpaceLine.h"
#include "Rocket.h"
#include "Enemy.h"
#include "Engine/Level.h"
#include "Particles/ParticleEmitter.h"

DEFINE_LOG_CATEGORY_STATIC(YourLog, Warning, All);
// Sets default values
ARocket::ARocket()
{
	
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	static ConstructorHelpers::FObjectFinder<UPaperFlipbook> Flipbook(TEXT("/Game/SpaceLine/Shots/Rocket/Rocket.Rocket"));
	GetRenderComponent()->SetFlipbook(Flipbook.Object);
	GetRenderComponent()->bGenerateOverlapEvents = true;
	GetRenderComponent()->SetNotifyRigidBodyCollision(true);
	GetRenderComponent()->SetCollisionProfileName(TEXT("MachineGunBullet"));


	static ConstructorHelpers::FObjectFinder<UParticleSystem> ParticleSystemRes(TEXT("/Game/SpaceLine/Particles/RocketWayparticles.RocketWayparticles"));
	ParticleEmitter = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Emiter"));
	ParticleEmitter->AttachTo(GetRenderComponent());
	ParticleEmitter->SetTemplate(ParticleSystemRes.Object);
	ParticleEmitter->SetRelativeLocation(FVector(0, 0, -100));

	static ConstructorHelpers::FObjectFinder<UParticleSystem> ParticleBlowRes(TEXT("/Game/SpaceLine/Particles/Blow.Blow"));
	ParticleBlow = ParticleBlowRes.Object;
	//ParticleEmitter->SetWorldRotation(FRotator(0, 0, 0));
}

// Called when the game starts or when spawned
void ARocket::BeginPlay()
{
	Super::BeginPlay();
	GetWorld()->GetTimerManager().SetTimer(KillTimer, this, &ARocket::Destroy, 5, false);
}

// Called every frame
void ARocket::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	AEnemy* Target = 0;
	float LastDist = 0;
	for (AActor* a : GetWorld()->GetCurrentLevel()->Actors)
	{
		AEnemy* tmp = Cast<AEnemy>(a);

		if (tmp)
		{
			if (LastDist == 0)
			{
				LastDist = FVector::Dist(a->GetActorLocation(), GetActorLocation());
				Target = Cast<AEnemy>(a);
			}
			if (FVector::Dist(a->GetActorLocation(), GetActorLocation()) < LastDist)
			{
				LastDist = FVector::Dist(a->GetActorLocation(), GetActorLocation());
				Target = Cast<AEnemy>(a);
			}
		}
	}
	
	
	AddActorLocalOffset(FVector(0, 0, 10 * 100 * DeltaTime),true);
	if (!Target)
		return;

	FRotator s = FRotationMatrix::MakeFromZ(GetActorLocation() - Target->GetActorLocation()).Rotator();
	
	FRotator d = s.Clamp();
	
	
		if (s.Yaw < GetActorRotation().Yaw)
			AddActorWorldRotation(FRotator(0, -0.2, 0), true);
		if (s.Yaw > GetActorRotation().Yaw)
			AddActorWorldRotation(FRotator(0, 0.2, 0), true);
	
	
	
	
		
}

void ARocket::Destroy()
{
	Super::Destroy();
}

void ARocket::NotifyHit(UPrimitiveComponent * MyComp, AActor * Other, UPrimitiveComponent * OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult & Hit)
{
	Super::NotifyHit(MyComp, Other, OtherComp, bSelfMoved, HitLocation, HitNormal, NormalImpulse, Hit);
	if (Cast<AEnemy>(Other) != nullptr)
	{
		Cast<AEnemy>(Other)->AddHealth(-3);
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ParticleBlow, GetActorLocation(), GetActorRotation());
		Destroy();
	}
}

