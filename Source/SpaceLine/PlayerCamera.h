// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "PlayerCamera.generated.h"
class APlayerSpriteShip;
UCLASS()
class SPACELINE_API APlayerCamera : public APawn
{
	GENERATED_BODY()

public:
	
	// Sets default values for this pawn's properties
	APlayerCamera();
	
	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	void PostLoad();

	void OnPressed();
	void OnReleased();
	void UpdatePlayerShipPos();
	bool ShipIsDead();
	bool ShipInitialized;
	UCameraComponent* CameraComponent;
	UAudioComponent* AudioComponent;
	USpringArmComponent* SpringArmComponent;

	USphereComponent* SphereComponent;
	
	TWeakObjectPtr<APlayerSpriteShip> PlayerShip;

	bool bMousePressed;
	
};
