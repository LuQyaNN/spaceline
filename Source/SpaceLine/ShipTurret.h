// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaperFlipbookActor.h"
#include "ShipTurret.generated.h"

/**
 * 
 */
UCLASS()
class SPACELINE_API AShipTurret : public APaperFlipbookActor
{
	GENERATED_BODY()
public:
		AShipTurret();

		UPaperFlipbookComponent* TurretGun;
		float PosCounter;
		void Tick(float delta) override;
		void MoveToPlayer();
		void Fire();

		UPROPERTY()
		TWeakObjectPtr<class AEnemy> GunTarget;


		TWeakObjectPtr<class APlayerSpriteShip> PlayerShip;
		FTimerHandle FireTimer;
		void RotateGun();
		void BeginPlay() override;
};
