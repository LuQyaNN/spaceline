// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Bullet.h"
#include "AlienBullet.generated.h"

/**
 * 
 */
UCLASS()
class SPACELINE_API AAlienBullet : public ABullet
{
	GENERATED_BODY()
public:
	AAlienBullet();

	virtual void OnSpawned(FVector Location, FRotator Direction) override;
	float SinMoveTime;
	void Tick(float delta) override;
	void NotifyHit(UPrimitiveComponent * MyComp, AActor * Other, UPrimitiveComponent * OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult & Hit)override;
	bool bSinMove;
};
