// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaperFlipbookActor.h"
#include "BonusAmmo.generated.h"

/**
 * 
 */
UCLASS()
class SPACELINE_API ABonusAmmo : public APaperFlipbookActor
{
	GENERATED_BODY()

public:
		ABonusAmmo();
		void Tick(float delta) override;

	void NotifyHit(UPrimitiveComponent * MyComp, AActor * Other, UPrimitiveComponent * OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult & Hit)override;

	
	
};
