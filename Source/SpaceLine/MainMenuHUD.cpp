// Fill out your copyright notice in the Description page of Project Settings.

#include "SpaceLine.h"
#include "MainMenuHUD.h"
#include "CanvasItem.h"
#include "Engine/Canvas.h"
#include "Kismet/GameplayStatics.h"

AMainMenuHUD::AMainMenuHUD()
{
	
	static ConstructorHelpers::FObjectFinder<UFont> FontRes(TEXT("/Game/SpaceLine/Font/BebasNeue.BebasNeue"));
	Font = FontRes.Object;
	Font->LegacyFontSize = 20;

	AudioComponent = CreateDefaultSubobject<UAudioComponent>(FName("AudioComponent"));
	AudioComponent->AttachTo(RootComponent);
	
	static ConstructorHelpers::FObjectFinder<USoundBase> SoundOnOverlapRes(TEXT("/Game/SpaceLine/Sounds/Btn_Cue.Btn_Cue"));
	
	AudioComponent->bAutoActivate = false;
	AudioComponent->SetSound(SoundOnOverlapRes.Object);
	
	
}

void AMainMenuHUD::DrawHUD()
{
	
	Super::DrawHUD();
	Font->LegacyFontSize = FMath::RoundToInt(Canvas->ClipX / 15);
	FCanvasTileItem NewGameBox(FVector2D(Canvas->ClipX / 2 - (Canvas->ClipX / 10 * 8) / 2, Canvas->ClipY / 10 * 2), FVector2D(Canvas->ClipX / 10 * 8, Canvas->ClipY / 10 * 1), FLinearColor::Gray);
	if (OverStartButton)
		NewGameBox.SetColor(FLinearColor(0.7, 0.7, 0.7));
	FCanvasTextItem NewGameText(FVector2D(Canvas->ClipX / 2, Canvas->ClipY / 20 * 5), FText::FromString("New Game"), Font, FLinearColor::White);
	NewGameText.bCentreX = true;
	NewGameText.bCentreY = true;
	Canvas->DrawItem(NewGameBox);
	Canvas->DrawItem(NewGameText);
	AddHitBox(FVector2D(Canvas->ClipX / 2 - (Canvas->ClipX / 10 * 8) / 2, Canvas->ClipY / 10 * 2), FVector2D(Canvas->ClipX / 10 * 8, Canvas->ClipY / 10 * 1), FName(*FString("New Game")), true);

	/*
	FCanvasTileItem OptionBox(FVector2D(Canvas->ClipX / 2 - (Canvas->ClipX / 10 * 8) / 2, Canvas->ClipY / 10 * 4), FVector2D(Canvas->ClipX / 10 * 8, Canvas->ClipY / 10 * 1), FLinearColor::Gray);
	FCanvasTextItem OptionText(FVector2D(Canvas->ClipX / 2, Canvas->ClipY / 20 * 9), FText::FromString("Option"), Font, FLinearColor::White);
	OptionText.bCentreX = true;
	OptionText.bCentreY = true;
	Canvas->DrawItem(OptionBox);
	Canvas->DrawItem(OptionText);
	AddHitBox(FVector2D(Canvas->ClipX / 2 - (Canvas->ClipX / 10 * 8) / 2, Canvas->ClipY / 10 * 4), FVector2D(Canvas->ClipX / 10 * 8, Canvas->ClipY / 10 * 1), FName(*FString("Option")), true);
	*/
	
	FCanvasTileItem ExitBox(FVector2D(Canvas->ClipX / 2 - (Canvas->ClipX / 10 * 8) / 2, Canvas->ClipY / 10 * 6), FVector2D(Canvas->ClipX / 10 * 8, Canvas->ClipY / 10 * 1), FLinearColor::Gray);
	if (OverExitButton)
		ExitBox.SetColor(FLinearColor(0.7, 0.7, 0.7));
	FCanvasTextItem ExitText(FVector2D(Canvas->ClipX / 2, Canvas->ClipY / 20 * 13), FText::FromString("Exit"), Font, FLinearColor::White);
	ExitText.bCentreX = true;
	ExitText.bCentreY = true;
	Canvas->DrawItem(ExitBox);
	Canvas->DrawItem(ExitText);
	AddHitBox(FVector2D(Canvas->ClipX / 2 - (Canvas->ClipX / 10 * 8) / 2, Canvas->ClipY / 10 * 6), FVector2D(Canvas->ClipX / 10 * 8, Canvas->ClipY / 10 * 1), FName(*FString("Exit")), true);
}

void AMainMenuHUD::NotifyHitBoxClick(FName BoxName)
{
	if (BoxName == FName(*FString("New Game")))
	{
		
	
		UGameplayStatics::OpenLevel(GetWorld(), FName(*FString("2DSideScrollerExampleMap")));
	}
	if (BoxName == FName(*FString("Exit")))
	{

		GetWorld()->GetFirstPlayerController()->ConsoleCommand(TEXT("quit"));
	}

}

void AMainMenuHUD::NotifyHitBoxBeginCursorOver(FName BoxName)
{
	if (BoxName == FName(*FString("New Game")))
	{
		OverStartButton = true;
		AudioComponent->Play();
	}

	if (BoxName == FName(*FString("Exit")))
	{

		OverExitButton = true;
		AudioComponent->Play();
	}
}

void AMainMenuHUD::NotifyHitBoxEndCursorOver(FName BoxName)
{
	if (BoxName == FName(*FString("New Game")))
	{
		OverStartButton = false;
	}

	if (BoxName == FName(*FString("Exit")))
	{

		OverExitButton = false;
	}
}

