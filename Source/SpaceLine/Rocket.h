// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaperFlipbookActor.h"
#include "Rocket.generated.h"

UCLASS()
class SPACELINE_API ARocket : public APaperFlipbookActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARocket();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	void Destroy();
	FTimerHandle KillTimer;
	UParticleSystemComponent* ParticleEmitter;
	UParticleSystem* ParticleBlow;
	void NotifyHit(UPrimitiveComponent * MyComp, AActor * Other, UPrimitiveComponent * OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult & Hit) override;
};
