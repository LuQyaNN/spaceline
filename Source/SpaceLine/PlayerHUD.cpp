// Fill out your copyright notice in the Description page of Project Settings.

#include "SpaceLine.h"
#include "PlayerHUD.h"
#include "PlayerSpriteShip.h"
#include "PlayerCamera.h"
#include "CanvasItem.h"
#include "Enemy.h"
#include "Engine/Canvas.h"
#include "SpaceLineGameMode.h"



APlayerHUD::APlayerHUD()
{
	
	static ConstructorHelpers::FObjectFinder<UTexture2D> ArmorTex(TEXT("/Game/SpaceLine/Bonuses/Armor/shield_PNG1259.shield_PNG1259"));
	ArmorTexRes = ArmorTex.Object->Resource;
	static ConstructorHelpers::FObjectFinder<UFont> FontRes(TEXT("/Game/SpaceLine/Font/BebasNeue.BebasNeue"));
	Font = FontRes.Object;
	Font->LegacyFontSize = 20;

	AudioComponent = CreateDefaultSubobject<UAudioComponent>(FName("AudioComponent"));
	AudioComponent->AttachTo(RootComponent);

	static ConstructorHelpers::FObjectFinder<USoundBase> SoundOnOverlapRes(TEXT("/Game/SpaceLine/Sounds/Btn_Cue.Btn_Cue"));

	AudioComponent->bAutoActivate = false;
	AudioComponent->SetSound(SoundOnOverlapRes.Object);
}

void APlayerHUD::DrawHUD()
{
	Font->LegacyFontSize = FMath::RoundToInt(Canvas->ClipX / 15 );
	Super::DrawHUD();
	int32 hp = 0;
	if (Cast<APlayerCamera>(GetOwningPawn())->PlayerShip != nullptr)
		hp = Cast<APlayerCamera>(GetOwningPawn())->PlayerShip->Health;
	
	

	FCanvasTileItem armor(FVector2D(Canvas->ClipX / 20 *2, Canvas->ClipY / 20 * 1), ArmorTexRes, FVector2D(Canvas->ClipX / 20 * 2, Canvas->ClipX / 20 * 2), FLinearColor::White);
	armor.BlendMode = ESimpleElementBlendMode::SE_BLEND_AlphaBlend;
	FCanvasTextItem ArmorCount(FVector2D(Canvas->ClipX / 20 * 4, Canvas->ClipY / 20 * 1), FText::FromString(FString::FromInt(hp)), Font, FLinearColor::White);
	Canvas->DrawItem(armor);
	Canvas->DrawItem(ArmorCount);
	if (LevelComplete || Cast<APlayerCamera>(GetOwningPawn())->PlayerShip == nullptr)
	{
		if (LevelComplete)
		{
			FCanvasTextItem WinText(FVector2D(Canvas->ClipX / 2, Canvas->ClipY / 20 * 9), FText::FromString("Level Complete"), Font, FLinearColor::White);
			WinText.bCentreX = true;
			WinText.bCentreY = true;
			Canvas->DrawItem(WinText);
		}
		if (Cast<APlayerCamera>(GetOwningPawn())->PlayerShip == nullptr)
		{
			FCanvasTextItem DeadText(FVector2D(Canvas->ClipX / 2, Canvas->ClipY / 20 * 9), FText::FromString("Game Over"), Font, FLinearColor::Red);
			DeadText.bCentreX = true;
			DeadText.bCentreY = true;
			Canvas->DrawItem(DeadText);
		}

		FCanvasTileItem RestartBox(FVector2D(Canvas->ClipX / 2 - (Canvas->ClipX / 10 * 6) / 2, Canvas->ClipY / 10 * 6), FVector2D(Canvas->ClipX / 10 * 6, Canvas->ClipY / 10 * 1), FLinearColor::Gray);
		if (OverRestartButton)
			RestartBox.SetColor(FLinearColor(0.7, 0.7, 0.7));
		FCanvasTextItem RestartText(FVector2D(Canvas->ClipX / 2, Canvas->ClipY / 20 * 13), FText::FromString("Restart"), Font, FLinearColor::White);
		RestartText.bCentreX = true;
		RestartText.bCentreY = true;
		Canvas->DrawItem(RestartBox);
		Canvas->DrawItem(RestartText);
		AddHitBox(FVector2D(Canvas->ClipX / 2 - (Canvas->ClipX / 10 * 6) / 2, Canvas->ClipY / 10 * 6), FVector2D(Canvas->ClipX / 10 * 6, Canvas->ClipY / 10 * 1), FName(*FString("Restart")), true);

		FCanvasTileItem MenuBox(FVector2D(Canvas->ClipX / 2 - (Canvas->ClipX / 10 * 6) / 2, Canvas->ClipY / 10 * 8), FVector2D(Canvas->ClipX / 10 * 6, Canvas->ClipY / 10 * 1), FLinearColor::Gray);
		if (OverMenuButton)
			MenuBox.SetColor(FLinearColor(0.7, 0.7, 0.7));
		FCanvasTextItem MenuText(FVector2D(Canvas->ClipX / 2, Canvas->ClipY / 20 * 17), FText::FromString("Menu"), Font, FLinearColor::White);
		MenuText.bCentreX = true;
		MenuText.bCentreY = true;
		Canvas->DrawItem(MenuBox);
		Canvas->DrawItem(MenuText);
		AddHitBox(FVector2D(Canvas->ClipX / 2 - (Canvas->ClipX / 10 * 6) / 2, Canvas->ClipY / 10 * 8), FVector2D(Canvas->ClipX / 10 * 6, Canvas->ClipY / 10 * 1), FName(*FString("Menu")), true);
	}
	
	/*
	for (int32 i = 0; i < Cast<ASpaceLineGameMode>(GetWorld()->GetAuthGameMode())->Enemys.Num(); ++i)
	{
		DrawHealth(Cast<ASpaceLineGameMode>(GetWorld()->GetAuthGameMode())->Enemys[i]);
	}
	*/
}

void APlayerHUD::DrawHealth(TWeakObjectPtr<class AEnemy> target)
{
	if (target.IsValid())
	{
		

		FVector2D OnScreenPos;
		GetOwningPlayerController()->ProjectWorldLocationToScreen(target->GetActorLocation(), OnScreenPos);

		

		FCanvasTileItem back(OnScreenPos, FVector2D(Canvas->ClipX / 10, Canvas->ClipY / 45), FLinearColor::Gray);
		
		FCanvasTileItem front(back.Position, FVector2D(back.Size.X * ((float)target->Health / (float)target->MaxHealth), back.Size.Y), FLinearColor::Green);
		
		Canvas->DrawItem(back);
		Canvas->DrawItem(front);
	}
}

void APlayerHUD::NotifyHitBoxClick(FName BoxName)
{
	if (BoxName == FName(*FString("Restart")))
	{

		UGameplayStatics::OpenLevel(GetWorld(), FName(*FString("2DSideScrollerExampleMap")));
	}
	if (BoxName == FName(*FString("Menu")))
	{

		UGameplayStatics::OpenLevel(GetWorld(), FName(*FString("MainMenu")));
	}
}

void APlayerHUD::NotifyHitBoxBeginCursorOver(FName BoxName)
{
	if (BoxName == FName(*FString("Restart")))
	{
		AudioComponent->Play();
		OverRestartButton = true;
	}
	if (BoxName == FName(*FString("Menu")))
	{
		AudioComponent->Play();
		OverMenuButton = true;
	}
}

void APlayerHUD::NotifyHitBoxEndCursorOver(FName BoxName)
{
	if (BoxName == FName(*FString("Restart")))
	{

		OverRestartButton = false;
	}
	if (BoxName == FName(*FString("Menu")))
	{

		OverMenuButton = false;
	}
}

