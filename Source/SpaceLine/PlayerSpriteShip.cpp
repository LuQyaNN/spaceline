// Fill out your copyright notice in the Description page of Project Settings.

#include "SpaceLine.h"
#include "PlayerSpriteShip.h"
#include "MachineGunBullet.h"
#include "Rocket.h"
#include "PlayerCamera.h"
#include "Enemy.h"
#include "Engine/Level.h"
#include "ShipTurret.h"

APlayerSpriteShip::APlayerSpriteShip()
{
	
	static ConstructorHelpers::FObjectFinder<UPaperFlipbook> SpriteTex(TEXT("/Game/SpaceLine/Ships/Player/F5S.F5S"));
	GetRenderComponent()->SetFlipbook(SpriteTex.Object);
	Sprite = SpriteTex.Object;
	GetRenderComponent()->SetWorldScale3D(FVector(3,3,3));
	
	static ConstructorHelpers::FObjectFinder<UPaperFlipbook> ImmortalSpriteTex(TEXT("/Game/SpaceLine/Ships/Player/F5S2_Immortal.F5S2_Immortal"));
	ImmortalSprite = ImmortalSpriteTex.Object;

	
	

	GetRenderComponent()->bGenerateOverlapEvents = true;
	GetRenderComponent()->SetNotifyRigidBodyCollision(true);
	GetRenderComponent()->SetCollisionProfileName(TEXT("PlayerShip"));
	Health = 3;
	
}

void APlayerSpriteShip::Fire()
{
	FVector loc = GetActorLocation() - FVector(0,0,1);
	FRotator rot = GetActorRotation();
	switch (WeaponLvl)
	{
	case 0:
	{
		GetWorld()->SpawnActor(AMachineGunBullet::StaticClass(), &loc, &rot);
		break;
	}
	case 1:
	{
		FRotator rot1 = rot + FRotator(0, 25, 0),
			rot2 = rot + FRotator(0, -25, 0);
		GetWorld()->SpawnActor(AMachineGunBullet::StaticClass(), &loc, &rot);
		GetWorld()->SpawnActor(AMachineGunBullet::StaticClass(), &loc, &rot1);
		GetWorld()->SpawnActor(AMachineGunBullet::StaticClass(), &loc, &rot2);
		break;
	}
	case 2:
	{

		FRotator rot1 = rot + FRotator(0, 25, 0),
			rot2 = rot + FRotator(0, -25, 0);
		GetWorld()->SpawnActor(AMachineGunBullet::StaticClass(), &loc, &rot);
		GetWorld()->SpawnActor(AMachineGunBullet::StaticClass(), &loc, &rot1);
		GetWorld()->SpawnActor(AMachineGunBullet::StaticClass(), &loc, &rot2);


		if(!GetWorld()->GetTimerManager().IsTimerActive(RocketFireTimer))
			GetWorld()->GetTimerManager().SetTimer(RocketFireTimer, this, &APlayerSpriteShip::FireRocket, 2, true);
		break;
	}
	case 3:
	{
		/*
		FRotator rot1 = rot + FRotator(0, 25, 0),
			rot2 = rot + FRotator(0, -25, 0);
		GetWorld()->SpawnActor(AMachineGunBullet::StaticClass(), &loc, &rot);
		GetWorld()->SpawnActor(AMachineGunBullet::StaticClass(), &loc, &rot1);
		GetWorld()->SpawnActor(AMachineGunBullet::StaticClass(), &loc, &rot2);
		*/
		if (!GetWorld()->GetTimerManager().IsTimerActive(RocketFireTimer))
			GetWorld()->GetTimerManager().SetTimer(RocketFireTimer, this, &APlayerSpriteShip::FireRocket, 2, true);
		/*
		AEnemy* Target = 0;
		float LastDist = 0;
		for (AActor* a : GetWorld()->GetCurrentLevel()->Actors)
		{
			AEnemy* tmp = Cast<AEnemy>(a);
			
			if (tmp)
			{
				if (LastDist == 0)
				{
					LastDist = FVector::Dist(a->GetActorLocation(), GetActorLocation());
					Target = Cast<AEnemy>(a);
				}
				if (FVector::Dist(a->GetActorLocation(), GetActorLocation()) < LastDist)
				{
					LastDist = FVector::Dist(a->GetActorLocation(), GetActorLocation());
					Target = Cast<AEnemy>(a);
				}
			}
		}
		
		if (!Target)
			return;
		FRotator s = FRotationMatrix::MakeFromZ(Target->GetActorLocation() - GetActorLocation()).Rotator();

		
		GetWorld()->SpawnActor(AMachineGunBullet::StaticClass(), &loc, &s);
		*/
		if (!turret.IsValid())
		{
			turret = (AShipTurret*)GetWorld()->SpawnActor(AShipTurret::StaticClass(), &loc, &rot);
			turret->PlayerShip = this;
		}
		break;
	}
	default:
		break;
	}
	
}

void APlayerSpriteShip::FireRocket()
{
	FVector loc = GetActorLocation() - FVector(0, 0, 1);
	FRotator rot = GetActorRotation();
	GetWorld()->SpawnActor(ARocket::StaticClass(), &loc, &rot);
}

bool APlayerSpriteShip::AddHealth(int32 num)
{

	int32 tmpHealth = Health;
	int32 tmpHealth2 = Health;
	tmpHealth2 += num;
	
	if (tmpHealth > tmpHealth2 && !bImmortality)
	{
		Health += num;
		EnableImmortality();
		GetWorld()->GetTimerManager().SetTimer(ImmortalityTimer, this, &APlayerSpriteShip::DisableImmortality, 2);
	}
	if (tmpHealth < tmpHealth2)
	{
		Health += num;
		
	}
	if (Health <= 0)
		Destroy();
	return true;
	
}

void APlayerSpriteShip::EnableImmortality()
{
	GetRenderComponent()->SetFlipbook(ImmortalSprite);
	bImmortality = true;
}

void APlayerSpriteShip::DisableImmortality()
{
	GetRenderComponent()->SetFlipbook(Sprite);
	bImmortality = false;
}

void APlayerSpriteShip::BeginPlay()
{
	Super::BeginPlay();
	GetWorld()->GetTimerManager().SetTimer(FireTimer, this, &APlayerSpriteShip::Fire, 0.5, true);
	
}

bool APlayerSpriteShip::AddWeaponLvl(int32 num)
{
	if (WeaponLvl < 3)
	{
		WeaponLvl += num;
		return true;
	}
	return false;
}



void APlayerSpriteShip::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);
	if (Cast<AEnemy>(OtherActor) != nullptr)
	{
		AddHealth(-1);

	}
}

bool APlayerSpriteShip::isDead()
{
	return Health <= 0;
}
