// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "SpaceLine.h"
#include "SpaceLineGameMode.h"
#include "SpaceLineCharacter.h"
#include "PlayerCamera.h"
#include "PlayerSpriteShip.h"
#include "EnemySpriteShipOne.h"
#include "PlayerHUD.h"
#include "BonusArmor.h"
#include "BonusAmmo.h"
#include "PlanetActor.h"
#include "EnemySpriteShipTwo.h"

ASpaceLineGameMode::ASpaceLineGameMode()
{
	DefaultPawnClass = APlayerCamera::StaticClass();
	// set default pawn class to our character
	HUDClass = APlayerHUD::StaticClass();
	
}

void ASpaceLineGameMode::BeginPlay()
{
	Super::BeginPlay();
	GetWorld()->GetTimerManager().SetTimer(StageTimer, this, &ASpaceLineGameMode::ExecStage, 1, true);
	FVector pos(3000, 1000, 10);
	GetWorld()->SpawnActor(APlanetActor::StaticClass(), &pos);
}

void ASpaceLineGameMode::PostLoad()
{
	Super::PostLoad();
	
}

void ASpaceLineGameMode::ExecStage()
{
	//CurrentStage = FMath::RandRange(1, 6);
	


	if (!Cast<APlayerCamera>(GetWorld()->GetFirstPlayerController()->GetPawn())->ShipIsDead())
	{
		switch (CurrentStage)
		{
		case 5:
		{

			FVector pos(2700, -1000, 100);
			FRotator rot(0, -90, -90);
			AEnemySpriteShipOne* ship = (AEnemySpriteShipOne*)GetWorld()->SpawnActor(AEnemySpriteShipOne::StaticClass(), &pos, &rot);
			ship->waypoints.Add(FVector(1000, -1000, 100));
			Enemys.Add(TWeakObjectPtr<AEnemy>(ship));

			pos = FVector(2700, 0, 100);
			rot = FRotator(0, 90, 90);
			ship = (AEnemySpriteShipOne*)GetWorld()->SpawnActor(AEnemySpriteShipOne::StaticClass(), &pos, &rot);
			ship->waypoints.Add(FVector(1000, 0, 100));
			ship->LockOnPlayerShip = true;
			ship->bSinFire = true;
			Enemys.Add(TWeakObjectPtr<AEnemy>(ship));

			pos = FVector(2700, 1000, 100);
			rot = FRotator(0, 90, 90);
			ship = (AEnemySpriteShipOne*)GetWorld()->SpawnActor(AEnemySpriteShipOne::StaticClass(), &pos, &rot);
			ship->waypoints.Add(FVector(1000, 1000, 100));
			Enemys.Add(TWeakObjectPtr<AEnemy>(ship));
			break;
		}
		case 10:
		{

			FVector pos(2700, 0, 100);
			FRotator rot(0, 90, 90);
			AEnemySpriteShipOne* ship = (AEnemySpriteShipOne*)GetWorld()->SpawnActor(AEnemySpriteShipOne::StaticClass(), &pos, &rot);
			ship->waypoints.Add(FVector(1200, 0, 100));
			ship->waypoints.Add(FVector(1500, 1000, 100));
			ship->waypoints.Add(FVector(1500, -1000, 100));
			Enemys.Add(TWeakObjectPtr<AEnemy>(ship));
			break;
		}
		case 15:
		{

			FVector pos(2700, 0, 100);
			FRotator rot(0, 90, 90);
			AEnemySpriteShipTwo* ship = (AEnemySpriteShipTwo*)GetWorld()->SpawnActor(AEnemySpriteShipTwo::StaticClass(), &pos, &rot);
			ship->waypoints.Add(FVector(1200, 0, 100));
			Enemys.Add(TWeakObjectPtr<AEnemy>(ship));

			break;
		}
		case 16:
		{

			FVector pos(2700, 0, 100);
			FRotator rot(0, -90, 90);
			GetWorld()->SpawnActor(ABonusAmmo::StaticClass(), &pos, &rot);

			break;
		}
		case 20:
		{

			FVector pos(2700, -1000, 100);
			FRotator rot(0, 90, 90);
			AEnemySpriteShipOne* ship = (AEnemySpriteShipOne*)GetWorld()->SpawnActor(AEnemySpriteShipOne::StaticClass(), &pos, &rot);
			ship->waypoints.Add(FVector(1400, -1000, 100));
			ship->LockOnPlayerShip = true;

			Enemys.Add(TWeakObjectPtr<AEnemy>(ship));

			pos = FVector(2700, -200, 100);
			rot = FRotator(0, 90, 90);
			ship = (AEnemySpriteShipOne*)GetWorld()->SpawnActor(AEnemySpriteShipOne::StaticClass(), &pos, &rot);
			ship->waypoints.Add(FVector(1400, -300, 100));
			ship->LockOnPlayerShip = true;
			Enemys.Add(TWeakObjectPtr<AEnemy>(ship));

			pos = FVector(2700, 200, 100);
			rot = FRotator(0, 90, 90);
			ship = (AEnemySpriteShipOne*)GetWorld()->SpawnActor(AEnemySpriteShipOne::StaticClass(), &pos, &rot);
			ship->waypoints.Add(FVector(1400, 300, 100));
			ship->LockOnPlayerShip = true;
			Enemys.Add(TWeakObjectPtr<AEnemy>(ship));

			pos = FVector(2700, 1000, 100);
			rot = FRotator(0, 90, 90);
			ship = (AEnemySpriteShipOne*)GetWorld()->SpawnActor(AEnemySpriteShipOne::StaticClass(), &pos, &rot);
			ship->waypoints.Add(FVector(1400, 1000, 100));
			ship->LockOnPlayerShip = true;
			Enemys.Add(TWeakObjectPtr<AEnemy>(ship));


			break;
		}

		case 25:
		{

			FVector pos(2700, 0, 100);
			FRotator rot(0, 90, 90);
			AEnemySpriteShipOne* ship = (AEnemySpriteShipOne*)GetWorld()->SpawnActor(AEnemySpriteShipOne::StaticClass(), &pos, &rot);
			ship->bMoveToPlayer = true;
			ship->TargetShift = FVector(3000, 0, 0);
			Enemys.Add(TWeakObjectPtr<AEnemy>(ship));


			break;
		}
		case 26:
		{

			FVector pos(2700, 0, 100);
			FRotator rot(0, -90, 90);
			GetWorld()->SpawnActor(ABonusArmor::StaticClass(), &pos, &rot);


			break;
		}
		case 28:
		{

			FVector pos(2700, 0, 100);
			FRotator rot(0, -90, 90);
			GetWorld()->SpawnActor(ABonusAmmo::StaticClass(), &pos, &rot);

			break;
		}
		case 35:
		{

			FVector pos(2700, 0, 100);
			FRotator rot(0, 90, 90);
			AEnemySpriteShipOne* ship = (AEnemySpriteShipOne*)GetWorld()->SpawnActor(AEnemySpriteShipOne::StaticClass(), &pos, &rot);
			ship->waypoints.Add(FVector(1000, -1000, 100));
			ship->waypoints.Add(FVector(2000, -1000, 100));
			ship->waypoints.Add(FVector(1000, 1000, 100));
			ship->waypoints.Add(FVector(2000, 1000, 100));
			Enemys.Add(TWeakObjectPtr<AEnemy>(ship));

			break;
		}
		case 40:
		{

			FVector pos(2700, 0, 100);
			FRotator rot(0, 90, 90);
			AEnemySpriteShipOne* ship = (AEnemySpriteShipOne*)GetWorld()->SpawnActor(AEnemySpriteShipOne::StaticClass(), &pos, &rot);
			ship->bMoveToPlayer = true;
			ship->TargetShift = FVector(3000, 0, 0);
			Enemys.Add(TWeakObjectPtr<AEnemy>(ship));

			pos = FVector(2700, 0, 100);
			rot = FRotator(0, 90, 90);
			ship = (AEnemySpriteShipOne*)GetWorld()->SpawnActor(AEnemySpriteShipOne::StaticClass(), &pos, &rot);
			ship->bMoveToPlayer = true;
			ship->TargetShift = FVector(2000, 0, 0);
			Enemys.Add(TWeakObjectPtr<AEnemy>(ship));

			pos = FVector(2700, 0, 100);
			rot = FRotator(0, 90, 90);
			ship = (AEnemySpriteShipOne*)GetWorld()->SpawnActor(AEnemySpriteShipOne::StaticClass(), &pos, &rot);
			ship->bMoveToPlayer = true;
			ship->TargetShift = FVector(1500, 0, 0);
			Enemys.Add(TWeakObjectPtr<AEnemy>(ship));

			break;
		}
		case 45:
		{

			FVector pos(2700, 0, 100);
			FRotator rot(0, -90, 90);
			GetWorld()->SpawnActor(ABonusAmmo::StaticClass(), &pos, &rot);

			break;
		}
		case 50:
		{

			FVector pos(2700, 0, 100);
			FRotator rot(0, 90, 90);
			AEnemySpriteShipOne* ship = (AEnemySpriteShipOne*)GetWorld()->SpawnActor(AEnemySpriteShipOne::StaticClass(), &pos, &rot);
			ship->bMoveToPlayer = true;
			ship->TargetShift = FVector(3000, 300, 0);
			Enemys.Add(TWeakObjectPtr<AEnemy>(ship));

			pos = FVector(2700, 0, 100);
			rot = FRotator(0, 90, 90);
			ship = (AEnemySpriteShipOne*)GetWorld()->SpawnActor(AEnemySpriteShipOne::StaticClass(), &pos, &rot);
			ship->bMoveToPlayer = true;
			ship->TargetShift = FVector(3000, 0, 0);
			Enemys.Add(TWeakObjectPtr<AEnemy>(ship));

			pos = FVector(2700, 0, 100);
			rot = FRotator(0, 90, 90);
			ship = (AEnemySpriteShipOne*)GetWorld()->SpawnActor(AEnemySpriteShipOne::StaticClass(), &pos, &rot);
			ship->bMoveToPlayer = true;
			ship->TargetShift = FVector(3000, -300, 0);
			Enemys.Add(TWeakObjectPtr<AEnemy>(ship));

			break;
		}
		case 55:
		{

			FVector pos(2700, -1000, 100);
			FRotator rot(0, 90, 90);
			AEnemySpriteShipOne* ship = (AEnemySpriteShipOne*)GetWorld()->SpawnActor(AEnemySpriteShipOne::StaticClass(), &pos, &rot);
			ship->waypoints.Add(FVector(1400, -1000, 100));
			ship->LockOnPlayerShip = true;
			Enemys.Add(TWeakObjectPtr<AEnemy>(ship));

			pos = FVector(2700, -200, 100);
			rot = FRotator(0, 90, 90);
			ship = (AEnemySpriteShipOne*)GetWorld()->SpawnActor(AEnemySpriteShipOne::StaticClass(), &pos, &rot);
			ship->waypoints.Add(FVector(1400, -300, 100));
			ship->LockOnPlayerShip = true;
			Enemys.Add(TWeakObjectPtr<AEnemy>(ship));

			pos = FVector(2700, 200, 100);
			rot = FRotator(0, 90, 90);
			ship = (AEnemySpriteShipOne*)GetWorld()->SpawnActor(AEnemySpriteShipOne::StaticClass(), &pos, &rot);
			ship->waypoints.Add(FVector(1400, 300, 100));
			ship->LockOnPlayerShip = true;
			Enemys.Add(TWeakObjectPtr<AEnemy>(ship));

			pos = FVector(2700, 1000, 100);
			rot = FRotator(0, 90, 90);
			ship = (AEnemySpriteShipOne*)GetWorld()->SpawnActor(AEnemySpriteShipOne::StaticClass(), &pos, &rot);
			ship->waypoints.Add(FVector(1400, 1000, 100));
			ship->LockOnPlayerShip = true;
			Enemys.Add(TWeakObjectPtr<AEnemy>(ship));


			break;
		}
		
		case 60:
		{

			FVector pos(2700, -1000, 100);
			FRotator rot(0, 90, 90);
			AEnemySpriteShipOne* ship = (AEnemySpriteShipOne*)GetWorld()->SpawnActor(AEnemySpriteShipOne::StaticClass(), &pos, &rot);
			ship->waypoints.Add(FVector(1000, -1000, 100));
			Enemys.Add(TWeakObjectPtr<AEnemy>(ship));


			pos = FVector(2700, 0, 100);
			rot = FRotator(0, 90, 90);
			ship = (AEnemySpriteShipOne*)GetWorld()->SpawnActor(AEnemySpriteShipOne::StaticClass(), &pos, &rot);
			ship->waypoints.Add(FVector(1000, 0, 100));
			ship->LockOnPlayerShip = true;
			ship->bSinFire = true;
			Enemys.Add(TWeakObjectPtr<AEnemy>(ship));

			pos = FVector(2700, 1000, 100);
			rot = FRotator(0, 90, 90);
			ship = (AEnemySpriteShipOne*)GetWorld()->SpawnActor(AEnemySpriteShipOne::StaticClass(), &pos, &rot);
			ship->waypoints.Add(FVector(1000, 1000, 100));
			Enemys.Add(TWeakObjectPtr<AEnemy>(ship));

			break;
		}

		default:
			break;
		}
	}

	for (int32 i = 0; i < Enemys.Num(); ++i)
	{
		if (!Enemys[i].IsValid())
		{
			Enemys.RemoveAt(i);
		}
	}

	if (CurrentStage > 60 && Enemys.Num() == 0 && !Cast<APlayerCamera>(GetWorld()->GetFirstPlayerController()->GetPawn())->ShipIsDead())
	{
		Cast<APlayerHUD>(GetWorld()->GetFirstPlayerController()->GetHUD())->LevelComplete = true;
	}

	//CurrentStage = FMath::RandRange(1, 10);
	/*
	switch (CurrentStage)
	{
	case 1:
	{

		FVector pos(2700, FMath::RandRange(-1500, 1500), 100);
		FRotator rot(0, -90, 90);
		GetWorld()->SpawnActor(ABonusArmor::StaticClass(), &pos, &rot);


		break;
	}
	case 2:
	{

		FVector pos(2700, FMath::RandRange(-1500, 1500), 100);
		FRotator rot(0, -90, 90);
		GetWorld()->SpawnActor(ABonusAmmo::StaticClass(), &pos, &rot);


		break;
	}
	default:
		break;
	}
	*/
	CurrentStage++;
}
