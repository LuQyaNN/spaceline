// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "PaperFlipbookActor.h"
#include "Enemy.generated.h"

/**
 * 
 */
UCLASS()
class SPACELINE_API AEnemy : public APaperFlipbookActor
{
	GENERATED_BODY()
public:
	AEnemy();
	virtual void Fire();
	int32 Health;
	int32 MaxHealth;
	void AddHealth(int32 num);
	int32 currentwaypoint;
	TArray<FVector> waypoints;
	FVector TargetShift;
	UAudioComponent* AudioComponent;

	USoundBase* OnDestoroySound;
	UParticleSystem* ParticleBlow;
	bool LockOnPlayerShip;
	virtual void Move();
	void Tick(float delta) override;
	bool bMoveToPlayer;
};
