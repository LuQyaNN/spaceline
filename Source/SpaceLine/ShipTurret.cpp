// Fill out your copyright notice in the Description page of Project Settings.

#include "SpaceLine.h"
#include "ShipTurret.h"
#include "PlayerSpriteShip.h"
#include "Enemy.h"
#include "Engine/Level.h"
#include "MachineGunBullet.h"


AShipTurret::AShipTurret()
{
	
	PrimaryActorTick.bCanEverTick = true;
	static ConstructorHelpers::FObjectFinder<UPaperFlipbook> ShipTurretSprite(TEXT("/Game/SpaceLine/Shots/Turret/turret-tex_Flipbook.turret-tex_Flipbook"));
	GetRenderComponent()->SetFlipbook(ShipTurretSprite.Object);
	GetRenderComponent()->SetWorldScale3D(FVector(0.2, 0.2, 0.2));
	//GetRenderComponent()->GetSocketLocation(FName("Socket_0"));

	static ConstructorHelpers::FObjectFinder<UPaperFlipbook> TurretGunSprite(TEXT("/Game/SpaceLine/Shots/Turret/turretgun_Flipbook.turretgun_Flipbook"));
	TurretGun = CreateDefaultSubobject<UPaperFlipbookComponent>("Gun");
	TurretGun->SetFlipbook(TurretGunSprite.Object);
	
	TurretGun->AttachTo(GetRenderComponent(), FName("Socket_0"));
	TurretGun->AddRelativeLocation(FVector(0, 0, 10));
	TurretGun->SetRelativeScale3D(FVector(2, 2, 2));
	TurretGun->SetWorldRotation(FRotator(0, 90, 90));
}

void AShipTurret::BeginPlay()
{
	Super::BeginPlay();
	GetWorld()->GetTimerManager().SetTimer(FireTimer, this, &AShipTurret::Fire, 0.5, true);

}

void AShipTurret::Tick(float delta)
{
	Super::Tick(delta);
	MoveToPlayer();
	RotateGun();
}

void AShipTurret::MoveToPlayer()
{
	if (!PlayerShip.IsValid())
		return;
	PosCounter += 0.01;

	FVector TargetPos; 
	TargetPos.Z = 100;
	TargetPos.X = PlayerShip->GetActorLocation().X + FMath::Cos(PosCounter) * 1000;
	TargetPos.Y = PlayerShip->GetActorLocation().Y + FMath::Sin(PosCounter) * 1000;
	AddActorWorldOffset(FVector(TargetPos - GetActorLocation()).GetSafeNormal() * 10, true);
}

void AShipTurret::RotateGun()
{
	if (!GunTarget.IsValid())
		return;

	FRotator s = FRotationMatrix::MakeFromZ(GunTarget->GetActorLocation() - GetActorLocation()).Rotator();
	//GetRenderComponent()->SetRelativeRotation(s + FRotator(0, 90, 0));
	TurretGun->SetWorldRotation(FRotator(0, 0 + s.Yaw + 90, 90));
}

void AShipTurret::Fire()
{
	if (!PlayerShip.IsValid())
		return;

	FVector loc = GetActorLocation();
	
	AEnemy* Target = 0;
	float LastDist = 0;
	for (AActor* a : GetWorld()->GetCurrentLevel()->Actors)
	{
		AEnemy* tmp = Cast<AEnemy>(a);

		if (tmp)
		{
			if (LastDist == 0)
			{
				LastDist = FVector::Dist(a->GetActorLocation(), GetActorLocation());
				Target = Cast<AEnemy>(a);
			}
			if (FVector::Dist(a->GetActorLocation(), GetActorLocation()) < LastDist)
			{
				LastDist = FVector::Dist(a->GetActorLocation(), GetActorLocation());
				Target = Cast<AEnemy>(a);
			}
		}
	}

	if (!Target)
		return;

	GunTarget = Target;
	FRotator s = FRotationMatrix::MakeFromZ(Target->GetActorLocation() - GetActorLocation()).Rotator();

	GetWorld()->SpawnActor(AMachineGunBullet::StaticClass(), &loc, &s);
}