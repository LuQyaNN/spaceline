// Fill out your copyright notice in the Description page of Project Settings.

#include "SpaceLine.h"
#include "MachineGunBullet.h"
#include "Enemy.h"
#include "Kismet/GameplayStatics.h"

AMachineGunBullet::AMachineGunBullet()
{
	PrimaryActorTick.bCanEverTick = true;
	static ConstructorHelpers::FObjectFinder<UPaperFlipbook> Flipbook(TEXT("/Game/SpaceLine/Shots/MachineGun/Bullet_Flipbook.Bullet_Flipbook"));
	GetRenderComponent()->SetFlipbook(Flipbook.Object);
	GetRenderComponent()->bGenerateOverlapEvents = true;
	GetRenderComponent()->SetNotifyRigidBodyCollision(true);
	GetRenderComponent()->SetCollisionProfileName(TEXT("MachineGunBullet"));
	
	static ConstructorHelpers::FObjectFinder<UParticleSystem> ParticleSystemRes(TEXT("/Game/SpaceLine/Particles/MachineGunHIt.MachineGunHIt"));
	ParticleSystem = ParticleSystemRes.Object;
	SetRootComponent(GetRenderComponent());
	
	static ConstructorHelpers::FObjectFinder<USoundBase> SoundShotRes(TEXT("/Game/SpaceLine/Sounds/player_shot_Cue.player_shot_Cue"));
	AudioComponent->SetSound(SoundShotRes.Object);
}

void AMachineGunBullet::OnSpawned(FVector Location, FRotator Direction)
{
	
}

void AMachineGunBullet::Tick(float delta)
{
	Super::Tick(delta);
	AddActorLocalOffset(FVector(0, 0, 30*100*delta),true);
}

void AMachineGunBullet::NotifyHit(UPrimitiveComponent * MyComp, AActor * Other, UPrimitiveComponent * OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult & Hit)
{
	Super::NotifyHit(MyComp, Other, OtherComp, bSelfMoved, HitLocation, HitNormal, NormalImpulse, Hit);
	if (Cast<AEnemy>(Other) != nullptr)
	{
		Cast<AEnemy>(Other)->AddHealth(-1);
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ParticleSystem, GetActorLocation(), GetActorRotation());

		Destroy();
	}
}




