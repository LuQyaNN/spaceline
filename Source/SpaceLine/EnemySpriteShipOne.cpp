// Fill out your copyright notice in the Description page of Project Settings.

#include "SpaceLine.h"
#include "EnemySpriteShipOne.h"
#include "AlienBullet.h"


// Sets default values
AEnemySpriteShipOne::AEnemySpriteShipOne()
{
	
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	static ConstructorHelpers::FObjectFinder<UPaperFlipbook> Sprite(TEXT("/Game/SpaceLine/Ships/Alien_1/alien.alien"));
	GetRenderComponent()->SetFlipbook(Sprite.Object);
	GetRenderComponent()->SetWorldScale3D(FVector(3, 3, 3));
	//SetActorRotation(FRotator(0, -90, -90));
	Health = 3;
	MaxHealth = 3;
	GetRenderComponent()->bGenerateOverlapEvents = true;
	GetRenderComponent()->SetNotifyRigidBodyCollision(true);

	
}

// Called when the game starts or when spawned
void AEnemySpriteShipOne::BeginPlay()
{
	Super::BeginPlay();
	GetWorld()->GetTimerManager().SetTimer(FireTimer, this, &AEnemySpriteShipOne::Fire, 1, true);
}

// Called every frame
void AEnemySpriteShipOne::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void AEnemySpriteShipOne::Fire()
{
	FVector loc = GetActorLocation();
	FRotator rot = GetActorRotation();
	AAlienBullet* bullet = (AAlienBullet*)GetWorld()->SpawnActor(AAlienBullet::StaticClass(), &loc, &rot);
	if (bSinFire)
	{
		bullet->bSinMove = true;
	}
}
