// Fill out your copyright notice in the Description page of Project Settings.

#include "SpaceLine.h"
#include "Enemy.h"
#include "PlayerSpriteShip.h"
#include "PlayerCamera.h"

AEnemy::AEnemy()
{
	GetRenderComponent()->bGenerateOverlapEvents = true;
	GetRenderComponent()->SetNotifyRigidBodyCollision(true);
	GetRenderComponent()->SetCollisionProfileName(TEXT("AlienShip"));

	static ConstructorHelpers::FObjectFinder<UParticleSystem> ParticleBlowRes(TEXT("/Game/SpaceLine/Particles/Blow.Blow"));
	ParticleBlow = ParticleBlowRes.Object;

	AudioComponent = CreateDefaultSubobject<UAudioComponent>(FName("AudioComponent"));
	AudioComponent->AttachTo(GetRenderComponent());
	AudioComponent->bStopWhenOwnerDestroyed = false;
	
	static ConstructorHelpers::FObjectFinder<USoundBase> SoundExplosionRes(TEXT("/Game/SpaceLine/Sounds/explodemini_Cue.explodemini_Cue"));
	OnDestoroySound = SoundExplosionRes.Object;
}

void AEnemy::Fire()
{

}

void AEnemy::Tick(float delta)
{
	Super::Tick(delta);
	Move();
}

void AEnemy::Move()
{
	if (bMoveToPlayer)
	{
		if (Cast<APlayerCamera>(GetWorld()->GetFirstPlayerController()->GetPawn())->PlayerShip != nullptr)
			AddActorWorldOffset(FVector(Cast<APlayerCamera>(GetWorld()->GetFirstPlayerController()->GetPawn())->PlayerShip->GetActorLocation() + TargetShift - GetActorLocation()).GetSafeNormal() * 10, true);
	}
	else
	{
		if (waypoints.Num() <= 0)
			return;

		if (FVector::Dist(GetActorLocation(), waypoints[currentwaypoint]) < 20)
		{
			++currentwaypoint;
		}
		if (waypoints.Num() == currentwaypoint)
			currentwaypoint = 0;
		AddActorWorldOffset(FVector(waypoints[currentwaypoint] - GetActorLocation()).GetSafeNormal() * 10, true);
	}

	if (LockOnPlayerShip)
	{
		if (Cast<APlayerCamera>(GetWorld()->GetFirstPlayerController()->GetPawn())->PlayerShip.IsValid())
		{
			FRotator s = FRotationMatrix::MakeFromZ(Cast<APlayerCamera>(GetWorld()->GetFirstPlayerController()->GetPawn())->PlayerShip->GetActorLocation() - GetActorLocation()).Rotator();
			SetActorRotation(s);
		}
	}
}

void AEnemy::AddHealth(int32 num)
{
	Health += num;
	if (Health <= 0)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ParticleBlow, GetActorLocation(), GetActorRotation());
		AudioComponent->SetSound(OnDestoroySound);
		AudioComponent->Play();
		Destroy();
		
	}
}




